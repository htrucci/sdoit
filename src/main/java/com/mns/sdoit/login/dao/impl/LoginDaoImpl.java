package com.mns.sdoit.login.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mns.sdoit.common.dao.BaseDao;
import com.mns.sdoit.login.dao.LoginDao;
import com.mns.sdoit.login.model.ComCodeModel;
import com.mns.sdoit.login.model.MemberModel;

@Repository
public class LoginDaoImpl extends BaseDao implements LoginDao {

	@SuppressWarnings("deprecation")
	@Override
	public int getMemberCnt(String empNo) throws Exception {
		return (Integer) getSqlMapClientTemplate().queryForObject("LOGIN.getMemberCnt", empNo);
	}

	@SuppressWarnings("deprecation")
	@Override
	public MemberModel getMemberInfo(String empNo, String pw) throws Exception {
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("empNo", empNo);
		paramMap.put("pw", pw);
		return (MemberModel)getSqlMapClientTemplate().queryForObject("LOGIN.getMemberInfo", paramMap);
	}
	@SuppressWarnings("deprecation")
	@Override
	public MemberModel getMemberInfoByAuto(String empNo) throws Exception {
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("empNo", empNo);
		return (MemberModel)getSqlMapClientTemplate().queryForObject("LOGIN.getMemberInfoByAuto", paramMap);
	}
	@SuppressWarnings("deprecation")
	@Override
	public void insertMemberJoin(String empNo, String name, String pw, String unit, String office, String team) throws Exception {
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("empNo", empNo);
		paramMap.put("name", name);
		paramMap.put("pw", pw);
		paramMap.put("unit", unit);
		paramMap.put("office", office);
		paramMap.put("team", team);
		getSqlMapClientTemplate().insert("LOGIN.insertMemberJoin", paramMap);
		
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<ComCodeModel> getComCode(String grpCd) throws Exception {
		// TODO 쿼리작업 해야함
		return (List<ComCodeModel>)getSqlMapClientTemplate().queryForList("LOGIN.getComCode", grpCd);
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<ComCodeModel> getMstCodeList(String parentCode) throws Exception {
		return getSqlMapClientTemplate().queryForList("LOGIN.getMstCodeList", parentCode);
	}

	@SuppressWarnings("deprecation")
	@Override
	public int getDuplicateIdCheck(String empNo) throws Exception {
		return (Integer) getSqlMapClientTemplate().queryForObject("LOGIN.getMemberCnt", empNo);
	}
}
