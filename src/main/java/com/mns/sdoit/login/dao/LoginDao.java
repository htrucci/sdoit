package com.mns.sdoit.login.dao;

import java.util.List;

import com.mns.sdoit.login.model.ComCodeModel;
import com.mns.sdoit.login.model.MemberModel;


public interface LoginDao  {
	
	/**
	 * empNo에 매핑된 회원 Count를 반환 한다.
	 * 
	 * @param empNo
	 * @return
	 * @throws Exception
	 */
	public int getMemberCnt( String empNo ) throws Exception;
	
	/**
	 * 회원 정보를 가져온다.
	 * 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public MemberModel getMemberInfo(String id, String pw) throws Exception;
	/**
	 * 회원 가입
	 * 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public void insertMemberJoin(String empNo, String name, String pw, String unit, String office, String team) throws Exception;
	
	public List<ComCodeModel> getComCode(String grpCd) throws Exception;
	
	/**
	 * 마스터 코드 목록 반환
	 * @param parentCode
	 * @return
	 * @throws Exception
	 */
	public List<ComCodeModel> getMstCodeList(String parentCode) throws Exception;

	/**
	 * ID 중복검사
	 * @param empNo
	 * @return
	 * @throws Exception
	 */
	public int getDuplicateIdCheck(String empNo) throws Exception;

	/**
	 * 회원 정보를 가져온다. (자동로그인)
	 * 
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public MemberModel getMemberInfoByAuto(String id) throws Exception;
}
