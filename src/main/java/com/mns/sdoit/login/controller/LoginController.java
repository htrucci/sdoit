package com.mns.sdoit.login.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mns.sdoit.login.model.ComCodeModel;
import com.mns.sdoit.login.model.MemberModel;
import com.mns.sdoit.login.service.LoginService;

@Controller
public class LoginController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private LoginService loginService;

	/**
	 * 최초 접큰 컨트롤러.
	 * 로그인 페이지를 반환 한다.
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = {"/login/loginView"}, method ={RequestMethod.GET, RequestMethod.POST})
	public String loginView( Model model ) throws Exception {
		logger.debug("## LoginController - loginView");
		
		return "index";
	}
	
	/**
	 * 로그인 Process.
	 * 
	 * @param empNo
	 * @param pw
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login/login", method ={RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody Map<String,Object> ajaxLogin( @RequestParam String empNo,
			@RequestParam String pw,
			HttpServletRequest request ) throws Exception {
		logger.debug("## LoginController - ajaxLogin || empNo :: "+empNo+" / pw :: "+pw);
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		int memberCnt = loginService.getMemberCnt(empNo);
		
		if( memberCnt == 1 ){
			
			if( loginService.getMemberValid(empNo, pw) ){
				loginService.setSessionToMemberInfo(empNo, pw, request);
				resultMap.put("RESULT", "SUCCESS");
			}else{
				resultMap.put("RESULT", "INVALID_PASSWORD");
			}
			
		}else{
			resultMap.put("RESULT", "ID_NOT_EXIST");
		}
		
		return resultMap;
	}
	/**
	 * 회원가입 View.
	 * 
	 * @param empNo
	 * @param pw
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login/joinView", method ={RequestMethod.GET, RequestMethod.POST})
	public String joinView( Model model ) throws Exception {
		logger.debug("## LoginController - joinView");
		
		return "signup";
	}
	
	/**
	 * parentCode에 해당하는 코드 목록 반환
	 * @param parentCode
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getCodeList")
	public @ResponseBody List<ComCodeModel> getCodeList(
		@RequestParam String parentCode) throws Exception {
		logger.debug("## LoginController - getCodeList");
		logger.debug("## parentCode: {}", parentCode);
		
		return loginService.getMstCodeList(parentCode);
	}
	
	/**
	 * 회원가입 Process.
	 * 
	 * @param empNo
	 * @param pw
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login/joinMember", method ={RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody Map<String,Object> ajaxJoin( @RequestParam String empNo,
			@RequestParam String pw, @RequestParam String name,@RequestParam String unit, @RequestParam String office, @RequestParam String team,
			HttpServletRequest request ) throws Exception {
		logger.debug("## LoginController - ajaxJoin || empNo :: "+empNo+" / name ::  "+name+" / pw :: "+pw+" / unit :: "+unit +" / office :: "+office +" / team :: "+team );
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		
		loginService.insertMemberJoin(empNo, name, pw, unit, office, team);
		
		resultMap.put("RESULT", "SUCCESS");
		return resultMap;
	}
	
	
	@RequestMapping("/logout")
	public String logout(HttpSession session) throws Exception {
		session.invalidate();
		
		return "index";
	}
	
	/**
	 * ID 체크 ajax
	 * @param empNo
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/duplicateIdCheck")
	public @ResponseBody Map<String,Object> duplicateIdCheck( @RequestParam String empNo, HttpServletRequest request ) throws Exception {
		logger.debug("## LoginController - duplicateIdCheck || empNo :: "+empNo);
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		try {
			int idCnt = loginService.getDuplicateIdCheck(empNo);
			if(idCnt == 0){
				resultMap.put("RESULT", "SUCCESS");
			}else{
				resultMap.put("RESULT", "ERROR");
			}
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("RESULT", "ERROR");
		}
		return resultMap;
	}
}
