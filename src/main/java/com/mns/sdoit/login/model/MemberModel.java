package com.mns.sdoit.login.model;

import lombok.Data;

@Data
public class MemberModel {
	
	private String empNo = "";
	private String pw = "";
	private String name = "";
	private String unitCode = "";
	private String officeCode = "";
	private String teamCode = "";
	
}