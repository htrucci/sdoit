package com.mns.sdoit.login.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mns.sdoit.login.dao.LoginDao;
import com.mns.sdoit.login.model.ComCodeModel;
import com.mns.sdoit.login.model.MemberModel;
import com.mns.sdoit.login.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

	@Autowired
	private LoginDao loginDao;
	
	@Override
	public int getMemberCnt(String empNo) throws Exception {
		logger.debug("## LoginServiceImpl - getMemberCnt");
		return loginDao.getMemberCnt(empNo);
	}

	@Override
	public boolean getMemberValid(String empNo, String pw) throws Exception {
		logger.debug("## LoginServiceImpl - getMemberInfo");
		return (loginDao.getMemberInfo(empNo, pw) != null )? true : false;
	}
	
	public void setSessionToMemberInfo(String empNo, String pw, HttpServletRequest request) throws Exception{
		MemberModel memberInfo = loginDao.getMemberInfo(empNo, pw);
		request.getSession().setAttribute("member", memberInfo);
		
	}
	public void setSessionAutoToMemberInfo(String empNo, HttpServletRequest request) throws Exception{
		MemberModel memberInfo = loginDao.getMemberInfoByAuto(empNo); 
		request.getSession().setAttribute("member", memberInfo);
		
	}
	@Override
	public void insertMemberJoin(String empNo, String name ,String pw, String unit, String office, String team) throws Exception {
		logger.debug("## LoginServiceImpl - insertMemberJoin");
		loginDao.insertMemberJoin(empNo, name, pw, unit, office, team);
		
	}

	@Override
	public List<ComCodeModel> getComCode(String grpCd) throws Exception {
		return loginDao.getComCode(grpCd);
	}

	@Override
	public List<ComCodeModel> getMstCodeList(String parentCode) throws Exception {
		return loginDao.getMstCodeList(parentCode);
	}

	@Override
	public int getDuplicateIdCheck(String empNo) throws Exception {
		return loginDao.getDuplicateIdCheck(empNo);
	}
	
}