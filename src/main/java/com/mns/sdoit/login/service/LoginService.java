package com.mns.sdoit.login.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.mns.sdoit.login.model.ComCodeModel;

public interface LoginService {
	
	/**
	 * empNo에 매핑된 회원 Count를 반환 한다.
	 * 
	 * @param empNo
	 * @return
	 * @throws Exception
	 */
	public int getMemberCnt( String empNo ) throws Exception;
	
	/**
	 * 회원 정보를 체크한다.
	 * 
	 * @param empNo
	 * @param pw
	 * @return
	 * @throws Exception
	 */
	public boolean getMemberValid(String empNo, String pw) throws Exception;
	
	/**
	 * 회원 정보를 가져와 세션에 정보를 세팅한다.
	 * 
	 * @param empNo
	 * @param pw
	 * @return
	 * @throws Exception
	 */
	public void setSessionToMemberInfo(String empNo, String pw, HttpServletRequest request) throws Exception;
	/**
	 * 회원 정보를 가져와 세션에 정보를 세팅한다.(자동로그인)
	 * 
	 * @param empNo
	 * @param pw
	 * @return
	 * @throws Exception
	 */
	public void setSessionAutoToMemberInfo(String empNo, HttpServletRequest request) throws Exception;

	/**
	 * 회원가입
	 * 
	 * @return
	 * @throws Exception
	 */
	public void insertMemberJoin(String empNo, String name, String pw, String unit, String office, String team) throws Exception;
	
	public List<ComCodeModel> getComCode( String grpCd ) throws Exception;

	/**
	 * 마스터 코드 목록 반환
	 * @param parentCode
	 * @return
	 * @throws Exception
	 */
	public List<ComCodeModel> getMstCodeList(String parentCode) throws Exception;

	/**
	 * ID 중복검사
	 * @param empNo
	 * @return
	 * @throws Exception
	 */
	public int getDuplicateIdCheck(String empNo) throws Exception;
}
