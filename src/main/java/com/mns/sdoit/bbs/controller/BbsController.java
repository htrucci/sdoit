package com.mns.sdoit.bbs.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mns.sdoit.bbs.model.BbsModel;
import com.mns.sdoit.bbs.service.BbsService;
import com.mns.sdoit.common.interceptor.LoginCheckInterceptor;
import com.mns.sdoit.common.service.CodeConstantService;
import com.mns.sdoit.common.spring.annotaion.InterceptorMapping;
import com.mns.sdoit.common.util.CommUtil;
import com.mns.sdoit.login.model.MemberModel;
import com.mns.sdoit.login.service.LoginService;

@Controller
public class BbsController {

	private static final Logger logger = LoggerFactory.getLogger(BbsController.class);

	@Autowired
	private BbsService bbsService;

	@Autowired
	private CodeConstantService codeConstantService;

	@Autowired
	private LoginService loginService;
	/**
	 * 최초 접근 컨트롤러.
	 * 메인 페이지를 반환 한다.
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = {"/","/index"}, method ={RequestMethod.GET, RequestMethod.POST})
	public String exView( HttpServletRequest request, Model model ) throws Exception {
		logger.debug("## LoginController - /");
		Cookie[] cookies = request.getCookies();
		MemberModel memberInfo = new MemberModel();
		String empNo = "";
		Map mapCookie = new HashMap();
		if(cookies!=null){			
			for(int i=0; i<cookies.length;i++){
				Cookie obj = cookies[i];
				mapCookie.put(obj.getName(), obj.getValue());
			}
			if((String)mapCookie.get("sdoit") != null){
				 empNo = (String) mapCookie.get("sdoit");
				 System.out.println(empNo);
				 loginService.setSessionAutoToMemberInfo(empNo, request);
				 return "redirect:/main";
			}			
			
		}
		return "index";
	}				

	/**
	 * 메인 페이지를 반환 한다.
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@InterceptorMapping(LoginCheckInterceptor.class)
	@RequestMapping(value = "/main", method ={RequestMethod.GET, RequestMethod.POST})
	public String mainView(
			@RequestParam(required = false, defaultValue="ALL") String filterParam 
			, HttpServletRequest request
			, Model model ) throws Exception {
		logger.debug("## BbsController - mainView / filterParam :: {} ", filterParam);

		HttpSession session		= request.getSession();
		MemberModel memberInfo	= (MemberModel)session.getAttribute("member");

		HashMap<String, String> param =  new HashMap<String, String>();
		param.put("empNo", memberInfo.getEmpNo());
		param.put("unitCode", memberInfo.getUnitCode());
		param.put("officeCode", memberInfo.getOfficeCode());
		param.put("teamCode", memberInfo.getTeamCode());
		param.put("filterParam", filterParam);
		
		// 글 리스트 (startDt / endDt 가 오늘 날짜에 들어가고 내가 신청하지 않은 글 리스트)
		List<BbsModel> everyPostNotMyList 	= bbsService.getEveryPostNotMyList(param);

		// 내가 신청한 글 리스트 (내가 신청한 글 리스트)
		List<BbsModel> everyPostMyList 		= bbsService.getEveryPostMyList(param);

		model.addAttribute("bbsListSize"	, everyPostNotMyList.size());
		model.addAttribute("bbsList"		, everyPostNotMyList);
		model.addAttribute("doItList"		, everyPostMyList);
		model.addAttribute("filterParam"	, filterParam);
		model.addAttribute("empNo", memberInfo.getEmpNo());
		return "bbs/mainView";
	}

	/**
	 * Do it 글 입력 폼화면이동
	 * @param model
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/writeBoardForm", method ={RequestMethod.GET, RequestMethod.POST})
	public String insertBoardForm(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.debug("## BbsController - insertBoardForm");
		return "bbs/writeBoard";
	}

	/**
	 * DO IT 글 등록
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/writeBoard", method ={RequestMethod.GET, RequestMethod.POST})
	public void insertBoard(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.debug("## BbsController - insertBoard");

		HttpSession session		= request.getSession();
		MemberModel memberInfo	= (MemberModel)session.getAttribute("member");
		String empNo 			= memberInfo.getEmpNo();
		String unit				= memberInfo.getUnitCode();
		String office			= memberInfo.getOfficeCode();
		String team				= memberInfo.getTeamCode();

		String openRange 		= CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "openRange", "ALL"));
		BbsModel bbs = new BbsModel();
		bbs.setTitle(CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "title", "제목없음")));
		bbs.setDescription(CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "description", "내용없음")));
		bbs.setRegMember(empNo);
		bbs.setStartDt(CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "startDt", "2015-01-01")));
		bbs.setEndDt(CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "endDt", "2015-12-31")));
		bbs.setPushYn(CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "pushYn", "N")));
		if("ALL".equals(openRange)){
			bbs.setOpenRange("MNS");
		}else if("UNIT".equals(openRange)){
			bbs.setOpenRange(unit);
		}else if("OFFICE".equals(openRange)){
			bbs.setOpenRange(office);
		}else if("TEAM".equals(openRange)){
			bbs.setOpenRange(team);
		}

		JSONObject json = new JSONObject();
		try {
			bbsService.insertBoard(bbs);
			json.put("resultCode"	, "00");
			json.put("resultMsg"	, "등록되었습니다.");
		} catch (Exception e) {
			json.put("resultCode"	, "99");
			json.put("resultMsg"	, "서버오류");
			e.printStackTrace();
		} finally {
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().print(json.toString());
			response.flushBuffer();
		}
	}


	/**
	 * 게시글 세부내용 보기 / 내 글 수정하기
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@InterceptorMapping(LoginCheckInterceptor.class)
	@RequestMapping(value = "/myBoardDetail", method ={RequestMethod.GET, RequestMethod.POST})
	public String myBoardDetail( Model model, HttpServletRequest request) throws Exception {
		logger.debug("## BbsController - myBoardDetail");

		HttpSession session		= request.getSession();
		MemberModel memberInfo	= (MemberModel)session.getAttribute("member");
		String empNo_session	= memberInfo.getEmpNo();

		String idx 				= CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "idx", ""));
		String updCmd 			= CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "updCmd", ""));

		BbsModel myBoardInfo = new BbsModel();
		try {
			myBoardInfo = bbsService.selectMyBoardInfo(Integer.parseInt(idx));
		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("errMsg", "글 불러오기 실패");
		}

		model.addAttribute("empNo_session", empNo_session);
		model.addAttribute("bbsInfo", myBoardInfo);

		if("U".equals(updCmd)){
			return "bbs/boardContentUpdView";
		}
		return "bbs/boardContentView";
	}

	/**
	 * 글 수정
	 * @param model
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@InterceptorMapping(LoginCheckInterceptor.class)
	@RequestMapping(value = "/updateBoard", method ={RequestMethod.GET, RequestMethod.POST})
	public void updateBoard(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.debug("## BbsController - updateBoard");

		HttpSession session		= request.getSession();
		MemberModel memberInfo	= (MemberModel)session.getAttribute("member");
		String empNo 			= memberInfo.getEmpNo();

		BbsModel bbs = new BbsModel();
		String idx 			= CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "idx", ""));
		String title 		= CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "title", ""));
		String description 	= CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "description", ""));
		String startDt		= CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "startDt", "2015-01-01"));
		String endDt		= CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "endDt", "2015-12-31"));

		bbs.setIdx(Integer.parseInt(idx));
		bbs.setTitle(title);
		bbs.setDescription(description);
		bbs.setRegMember(empNo);
		bbs.setStartDt(startDt.replace("-", ""));
		bbs.setEndDt(endDt.replace("-", ""));

		JSONObject json = new JSONObject();
		try {
			bbsService.updateBoard(bbs);
			json.put("resultCode"	, "00");
			json.put("resultMsg"	, "수정되었습니다.");
		} catch (Exception e) {
			json.put("resultCode"	, "99");
			json.put("resultMsg"	, "서버오류");
		} finally {
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().print(json.toString());
			response.flushBuffer();
		}
	}

	/**
	 * 글 삭제
	 * @param model
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@InterceptorMapping(LoginCheckInterceptor.class)
	@RequestMapping(value = "/deleteBoard", method ={RequestMethod.GET, RequestMethod.POST})
	public void deleteBoard(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.debug("## BbsController - deleteBoard");

		String idx = CommUtil.xssString(ServletRequestUtils.getStringParameter(request, "idx", ""));
		JSONObject json = new JSONObject();
		try {
			bbsService.deleteBoard(Integer.parseInt(idx));
			json.put("resultCode"	, "00");
			json.put("resultMsg"	, "삭제되었습니다.");
		} catch (Exception e) {
			json.put("resultCode"	, "99");
			json.put("resultMsg"	, "서버오류");
		} finally {
			response.setContentType("text/html;charset=UTF-8");
			response.getWriter().print(json.toString());
			response.flushBuffer();
		}
	}

	// 매핑 추가
	@InterceptorMapping(LoginCheckInterceptor.class)
	@RequestMapping(value = "/insertDo", method =RequestMethod.POST)
	public @ResponseBody Map<String, Object> insertDo(
			@RequestParam int bbsIdx
			, HttpServletRequest request) throws Exception {
		logger.debug("## BbsController - insertDo");
		logger.debug("## bbsIdx: {} / ", bbsIdx);

		Map<String,Object> resultMap = new HashMap<String,Object>();

		MemberModel memberInfo	= (MemberModel)request.getSession().getAttribute("member");
		String empNo 			= memberInfo.getEmpNo();
		logger.debug("## empNo: {} / ", empNo);

		try{
			bbsService.insertDo(bbsIdx, empNo);
			resultMap.put("RESULT", "SUCCESS");
		}catch(Exception e) {
			e.printStackTrace();
			resultMap.put("RESULT", "ERROR");
		}

		return resultMap;
	}

	// 매핑 삭제
	@RequestMapping(value = "/cancelDo", method =RequestMethod.POST)
	public @ResponseBody Map<String, Object> cancelDo(
			@RequestParam int bbsIdx
			, HttpServletRequest request) throws Exception {
		logger.debug("## BbsController - cancelDo");
		logger.debug("## bbsIdx: {} / ", bbsIdx);

		Map<String,Object> resultMap = new HashMap<String,Object>();

		MemberModel memberInfo	= (MemberModel)request.getSession().getAttribute("member");
		String empNo 			= memberInfo.getEmpNo();
		logger.debug("## empNo: {} / ", empNo);

		try{
			bbsService.deleteDo(bbsIdx, empNo);
			resultMap.put("RESULT", "SUCCESS");
		}catch(Exception e) {
			resultMap.put("RESULT", "ERROR");
		}

		return resultMap;
	}
	// 내가 쓴 글 리스트
	@RequestMapping(value="/getMyBbsList")
	public String getMyBbsList(
			HttpServletRequest request
			, Model model) throws Exception {
		logger.debug("## BbsController - getMyBbsList");

		MemberModel memberInfo	= (MemberModel)request.getSession().getAttribute("member");
		String empNo 			= memberInfo.getEmpNo();
		logger.debug("## empNo: {} / ", empNo);

		model.addAttribute("myBbsList", bbsService.getMyBbsList(empNo));

		return "myBbs/index";
	}

	// 신청자 목록
	@RequestMapping(value="/getApplicantList")
	public String getApplicantList(
			int bbsIdx
			, Model model) throws Exception {
		logger.debug("## BbsController - getApplicantList");

		model.addAttribute("applicantList", bbsService.getApplicantList(bbsIdx));

		return "myBbs/applicant";
	}
}
