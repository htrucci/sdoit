package com.mns.sdoit.bbs.model;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class BbsModel {
	
	private int idx;
	private String title = "";
	private String description = "";
	private String regMember = "";
	private String startDt = "";
	private String endDt = "";
	private Timestamp regDt = null;
	private Timestamp uptDt = null;
	private String bbsTypeCd = "";
	private String bbsTypeNm = "";
	private String regMamberName = "";
	private String openRange = "";
	private String pushYn = "";
	private int doCnt = 0;
	
}
