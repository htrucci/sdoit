package com.mns.sdoit.bbs.model;

import lombok.Data;

/**
 * BBS_REL_DO Model
 * @author 201410062
 *
 */
@Data
public class BbsRelDoModel{
	
	private String empNo = null;
	private int bbsIdx = 0;
	private String unitCode = "";
	private String officeCode = "";
	private String teamCode = "";
	private String memberName = "";
	private String teamName = "";
	
}
