package com.mns.sdoit.bbs.service;

import java.util.HashMap;
import java.util.List;

import com.mns.sdoit.bbs.model.BbsModel;
import com.mns.sdoit.bbs.model.BbsRelDoModel;

public interface BbsService {

	/**
	 * 내가 신청하지 않은 글 리스트
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<BbsModel> getEveryPostNotMyList(HashMap<String, String> param) throws Exception;

	/**
	 * 내가 신청한 글 리스트
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<BbsModel> getEveryPostMyList(HashMap<String, String> param) throws Exception;

	/**
	 * 글 등록
	 * @param bbs
	 * @throws Exception
	 */
	public void insertBoard(BbsModel bbs) throws Exception;

	/**
	 * 글 삭제
	 * @param idx
	 */
	public void deleteBoard(int idx) throws Exception;

	/**
	 * 글 내용 보기
	 * @param parseInt
	 * @return
	 * @throws Exception
	 */
	public BbsModel selectMyBoardInfo(int idx) throws Exception;

	/**
	 * 글 수정하기
	 * @param bbs
	 * @throws Exception
	 */
	public void updateBoard(BbsModel bbs) throws Exception;
	
	/**
	 * 신청한 정보 저장
	 * @param bbxIdx
	 * @param empNo
	 * @throws Exception
	 */
	public void insertDo(int bbxIdx, String empNo) throws Exception;
	
	public void deleteDo(int bbxIdx, String empNo) throws Exception;
	
	/**
	 * empNo가 작성한 bbs 목록 반환
	 * @param empNo
	 * @return
	 * @throws Exception
	 */
	public List<BbsModel> getMyBbsList(String empNo) throws Exception;
	
	/**
	 * bbsIdx에 해당하는 신청자 정보 목록 반환
	 * @param bbsIdx
	 * @return
	 * @throws Exception
	 */
	public List<BbsRelDoModel> getApplicantList(int bbsIdx) throws Exception;

	/**
	 * 게시판 타입 리스트가져오기
	 * @return
	 * @throws Exception
	 */
	public List<BbsModel> getBbsType(String grpCd) throws Exception;
	
}
