package com.mns.sdoit.bbs.service.impl;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mns.sdoit.bbs.dao.BbsDao;
import com.mns.sdoit.bbs.model.BbsModel;
import com.mns.sdoit.bbs.model.BbsRelDoModel;
import com.mns.sdoit.bbs.service.BbsService;
import com.mns.sdoit.push.model.PushModel;
import com.mns.sdoit.push.service.PushService;

@Service
public class BbsServiceImpl implements BbsService{

	private static final Logger logger = LoggerFactory.getLogger(BbsServiceImpl.class);
	
	@Autowired
	private BbsDao bbsDao;
	
	@Autowired
	private PushService pushService;
	/**
	 * 내가 신청하지 않은 글 리스트
	 */
	@Override
	public List<BbsModel> getEveryPostNotMyList(HashMap<String, String> param) throws Exception {
		logger.debug("## BbsServiceImpl - getEveryPostNotMyList");
		return bbsDao.getEveryPostNotMyList(param);
	}

	/**
	 * 내가 신청한 글 리스트
	 */
	@Override
	public List<BbsModel> getEveryPostMyList(HashMap<String, String> param)	throws Exception {
		logger.debug("## BbsServiceImpl - getEveryPostMyList");
		return bbsDao.getEveryPostMyList(param);
	}

	/**
	 * 글 등록
	 */
	@Override
	public void insertBoard(BbsModel bbs) throws Exception {
		logger.debug("## BbsServiceImpl - insertBoard");
		bbsDao.insertBoard(bbs);
		// bbs.getPushYn()이 Y일 경우에는 Push 보내는 로직 동작
		if(bbs.getPushYn().equals("Y")){
			List<PushModel> list = null;
			HashMap<String, String> param = new HashMap<String, String>();
			param.put("openRange", bbs.getOpenRange());
			list = 	pushService.selectPushTgtList(param);
			if(!list.isEmpty()){
				
				PushModel pushModel = new PushModel();
				for(int i=0; i<list.size(); i++){
					pushModel.setSndTgtTyp("MEMBER");
					pushModel.setSndTgt(list.get(i).getAppPushKey());
					pushModel.setOsTyp(list.get(i).getOsAffltn());
					pushModel.setPushTitl(bbs.getTitle());
					pushModel.setPushCnts(bbs.getDescription());
					pushModel.setSndSts("1");
					pushModel.setSndQty("1");
					pushModel.setRegrId("관리자");
					pushModel.setPushSndYn("Y");
					pushService.insertPushSndInfo(pushModel);
				}
			}
		}
	}

	/**
	 * 글 삭제
	 */
	@Override
	public void deleteBoard(int idx) throws Exception {
		logger.debug("## BbsServiceImpl - deleteBoard");
		bbsDao.deleteBoard(idx);
	}

	/**
	 * 글 내용 보기
	 */
	@Override
	public BbsModel selectMyBoardInfo(int idx) throws Exception {
		logger.debug("## BbsServiceImpl - selectMyBoardInfo");
		return bbsDao.selectMyBoardInfo(idx);
	}

	/**
	 * 글 수정 하기
	 */
	@Override
	public void updateBoard(BbsModel bbs) throws Exception {
		logger.debug("## BbsServiceImpl - updateBoard");
		bbsDao.updateBoard(bbs);
	}
	
	@Override
	public void insertDo(int bbxIdx, String empNo) throws Exception {
		bbsDao.insertDo(bbxIdx, empNo);
	}
	
	
	@Override
	public void deleteDo(int bbxIdx, String empNo) throws Exception {
		bbsDao.deleteDo(bbxIdx, empNo);
	}

	@Override
	public List<BbsModel> getMyBbsList(String empNo) throws Exception {
		return bbsDao.getMyBbsList(empNo);
	}

	@Override
	public List<BbsRelDoModel> getApplicantList(int bbsIdx) throws Exception {
		return bbsDao.getApplicantList(bbsIdx);
	}

	@Override
	public List<BbsModel> getBbsType(String grpCd) throws Exception {
		return bbsDao.getBbsType(grpCd);
	}
}
