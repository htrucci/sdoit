package com.mns.sdoit.bbs.dao;

import java.util.HashMap;
import java.util.List;

import com.mns.sdoit.bbs.model.BbsModel;
import com.mns.sdoit.bbs.model.BbsRelDoModel;

public interface BbsDao {

	/**
	 * 내가 신청하지 않은 글 리스트
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<BbsModel> getEveryPostNotMyList(HashMap<String, String> param) throws Exception;

	/**
	 * 내가 신청한 글 리스트
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<BbsModel> getEveryPostMyList(HashMap<String, String> param) throws Exception;

	/**
	 * 글 등록
	 * @param bbs
	 * @throws Exception
	 */
	public void insertBoard(BbsModel bbs) throws Exception;

	/**
	 * 글 삭제
	 * @param idx
	 * @throws Exception
	 */
	public void deleteBoard(int idx) throws Exception;

	/**
	 * 글 내용 보기
	 * @param idx
	 * @return
	 * @throws Exception
	 */
	public BbsModel selectMyBoardInfo(int idx) throws Exception;

	/**
	 * 글 수정 하기
	 * @param bbs
	 * @throws Exception
	 */
	public void updateBoard(BbsModel bbs) throws Exception;
	
	/**
	 * 신청한 정보 저장
	 * @param bbsIdx
	 * @param empNo
	 * @throws Exception
	 */
	public void insertDo(int bbsIdx, String empNo) throws Exception;
	
	public void deleteDo(int bbsIdx, String empNo) throws Exception;
	
	/**
	 * empNo가 작성한 bbs 목록 반환
	 * @param empNo
	 * @return
	 * @throws Exception
	 */
	public List<BbsModel> getMyBbsList(String empNo) throws Exception;
	
	/**
	 * bbxIdx에 해당하는 신청자 정보 목록 반환
	 * @param bbsIdx
	 * @return
	 * @throws Exception
	 */
	public List<BbsRelDoModel> getApplicantList(int bbsIdx) throws Exception;

	/**
	 * 게시판타입 리스트 가져오기
	 * @return
	 * @throws Exception
	 */
	public List<BbsModel> getBbsType(String grpCd) throws Exception;
	
}
