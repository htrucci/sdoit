package com.mns.sdoit.bbs.dao.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mns.sdoit.bbs.dao.BbsDao;
import com.mns.sdoit.bbs.model.BbsModel;
import com.mns.sdoit.bbs.model.BbsRelDoModel;
import com.mns.sdoit.common.dao.BaseDao;

@Repository
public class BbsDaoImpl extends BaseDao implements BbsDao{

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<BbsModel> getEveryPostNotMyList(HashMap<String, String> param) throws SQLException {
		return (List<BbsModel>) getSqlMapClientTemplate().queryForList("BBS.selectEveryPostNotMyList", param);
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<BbsModel> getEveryPostMyList(HashMap<String, String> param)	throws SQLException {
		return (List<BbsModel>) getSqlMapClientTemplate().queryForList("BBS.selectEveryPostMyList", param);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void insertBoard(BbsModel bbs) throws Exception {
		getSqlMapClientTemplate().insert("BBS.insertBoard", bbs);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void deleteBoard(int idx) throws Exception {
		getSqlMapClientTemplate().update("BBS.deleteBoard", idx);
	}

	@SuppressWarnings("deprecation")
	@Override
	public BbsModel selectMyBoardInfo(int idx) throws Exception {
		return (BbsModel) getSqlMapClientTemplate().queryForObject("BBS.selectMyBoardInfo", idx);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void updateBoard(BbsModel bbs) throws Exception {
		getSqlMapClientTemplate().update("BBS.updateBoardInfo", bbs);
	}
	
	@Override
	public void insertDo(int bbsIdx, String empNo) throws Exception {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("bbsIdx", bbsIdx);
		paramMap.put("empNo", empNo);
		getSqlMapClientTemplate().insert("BBS.insertDo", paramMap);
	}
	
	@SuppressWarnings({ "deprecation" })
	public void deleteDo(int bbsIdx, String empNo) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("bbsIdx", bbsIdx);
		paramMap.put("empNo", empNo);
		getSqlMapClientTemplate().delete("BBS.deleteDo", paramMap);
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<BbsModel> getMyBbsList(String empNo) throws Exception {
		return getSqlMapClientTemplate().queryForList("BBS.getMyBbsList", empNo);
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public List<BbsRelDoModel> getApplicantList(int bbsIdx) throws Exception {
		return getSqlMapClientTemplate().queryForList("BBS.getApplicantList", bbsIdx);
	}

	@SuppressWarnings({ "deprecation", "unchecked" })
	@Override
	public List<BbsModel> getBbsType(String grpCd) throws Exception {
		return (List<BbsModel>) getSqlMapClientTemplate().queryForList("BBS.getBbsType");
	}
	
}
