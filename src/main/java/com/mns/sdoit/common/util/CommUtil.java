package com.mns.sdoit.common.util;

public final class CommUtil {
	
	/**
	 * 특수문자 필터링 / SQL Injection 필터링
	 * @param str
	 * @return
	 */
	public static String xssString(String str) {
		if(str != null) 
		{
			str = str.replaceAll("#", "&#35");
			str = str.replaceAll("&", "&amp;");
			str = str.replaceAll("<", "&lt;");
			str = str.replaceAll(">", "&gt;");
			str = str.replaceAll("\"", "&quot;");
			str = str.replaceAll("\\(", "&#40");
			str = str.replaceAll("\\)", "&#41");
			str = str.replaceAll("'", "&#39");
			str = str.replaceAll("", "");
			return str.trim();
		}
		else
			return null;
	}
	
	/**
	 * String형 숫자를 int형 숫자로 변환
	 * @param s
	 * @return
	 */
	public static int parseInt(String s) {
		try {
			if(s == null || "".equals(s))
				return 0;
			else
				return Integer.parseInt(s);
		} catch (Exception e) {
			return 0;
		}
	}
}
