package com.mns.sdoit.common.util;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.ResourceBundle;

public class PropertyReader {
	
	private static ResourceBundle resource;
	
	public PropertyReader(String file) throws Exception {
		resource = ResourceBundle.getBundle(file);
	}
	
	public String getProperty(String key){
		return resource.getString(key).trim();
	}

	public HashMap<String, String> getHashMap() {
		HashMap<String, String> hMap = new HashMap<String, String>();
		
		@SuppressWarnings("rawtypes")
		Enumeration keys = resource.getKeys();
		String key 		= null;
		String value 	= null;
		
		while (keys.hasMoreElements()) {
			key 	= (String) keys.nextElement();
			value 	= resource.getString(key);
			hMap.put(key, value.trim());
		}
		return hMap;
	}
}
