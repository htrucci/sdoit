package com.mns.sdoit.common.service;

public interface CodeConstantService {
	
	public Object getValue(String key);
	
	public String getStringValue(String key);
	
	public int getIntValue(String key);
}
