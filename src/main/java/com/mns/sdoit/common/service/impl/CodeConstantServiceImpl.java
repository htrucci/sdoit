package com.mns.sdoit.common.service.impl;

import java.util.HashMap;

import org.springframework.stereotype.Service;

import com.mns.sdoit.common.service.CodeConstantService;
import com.mns.sdoit.common.util.PropertyReader;

@Service
public class CodeConstantServiceImpl implements CodeConstantService {

	PropertyReader 			m_pr			= null;
	HashMap<String, String> m_ArgumentMap 	= null;
	
	public CodeConstantServiceImpl() throws Exception {
		m_pr			= new PropertyReader("CodeConstant");
		m_ArgumentMap 	= m_pr.getHashMap();
	}
	
	@Override
	public Object getValue(String key) {
		return m_ArgumentMap.get(key);
	}

	@Override
	public String getStringValue(String key) {
		return String.valueOf(m_ArgumentMap.get(key));
	}

	@Override
	public int getIntValue(String key) {
		return 0;
	}
	
	
}
