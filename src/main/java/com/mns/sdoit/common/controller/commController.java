package com.mns.sdoit.common.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpServerErrorException;

import com.mns.sdoit.login.controller.LoginController;
import com.mns.sdoit.login.model.MemberModel;

@Controller
public class commController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@RequestMapping(value = {"/goCertPage"}, method ={RequestMethod.GET, RequestMethod.POST})
	public String goCertPage(Model model, HttpServletRequest request, HttpServletResponse response
			, @RequestParam String returnURL) throws Exception {
		logger.debug("## commController - goCertPage");
		
		//세션체크등
		return "redirect:"+returnURL;
	}
	
	@RequestMapping(value = {"/checkSession"}, method ={RequestMethod.GET, RequestMethod.POST})
	public void checkSession(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.debug("## commController - checkSession");
		
		JSONObject json = new JSONObject();
		try {
			HttpSession session		= request.getSession();
			MemberModel memberInfo	= (MemberModel)session.getAttribute("member");
			String empNo 			= memberInfo.getEmpNo();

			if(!"".equals(empNo) && empNo != null) {
				json.put("resultCode"	, "00");
				json.put("resultMsg"	, "세션확인");
			}else{
				json.put("resultCode"	, "11");
				json.put("resultMsg"	, "세션끊김");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			json.put("resultCode"	, "99");
			json.put("resultMsg"	, "서버오류");
		}
		
		response.setContentType("text/html;charset=UTF-8");
		response.getWriter().print(json.toString());
		response.flushBuffer();
		
	}
	
}
