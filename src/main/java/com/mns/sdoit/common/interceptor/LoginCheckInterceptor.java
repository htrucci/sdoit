package com.mns.sdoit.common.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.mns.sdoit.common.spring.annotaion.Interceptor;

@Interceptor
public class LoginCheckInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory.getLogger(LoginCheckInterceptor.class);
	
	@Override
	public boolean preHandle(
		HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		logger.debug("## LoginCheckInterceptor - preHandle");
		
		if( request.getSession().getAttribute("member") != null ){
			return super.preHandle(request, response, handler);
		}
		
		response.sendRedirect("/");
		return false;
	}
	
}
