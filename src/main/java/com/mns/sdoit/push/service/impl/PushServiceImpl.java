package com.mns.sdoit.push.service.impl;


import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mns.sdoit.bbs.dao.BbsDao;
import com.mns.sdoit.login.dao.LoginDao;
import com.mns.sdoit.login.model.MemberModel;
import com.mns.sdoit.login.service.LoginService;
import com.mns.sdoit.push.dao.PushDao;
import com.mns.sdoit.push.model.PushModel;
import com.mns.sdoit.push.service.PushService;

@Service
public class PushServiceImpl implements PushService {

	@Autowired
	private PushDao pushDao;
	
	@Override
	public void insertAppInfo(PushModel pushModel) {
		// TODO Auto-generated method stub
		pushDao.insertAppInfo(pushModel);
	}

	@Override
	public List<PushModel> selectPushTgtList(HashMap<String, String> param) {
		return pushDao.selectPushTgtList(param); 
	}

	@Override
	public void insertPushSndInfo(PushModel pushModel) {
		// TODO Auto-generated method stub
		pushDao.insertPushSndInfo(pushModel);
	}

	
}