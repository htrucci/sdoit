package com.mns.sdoit.push.model;


public class PushModel {

	private String appPushKey = "";
	private String frstConnDt = "";
	private String osAffltn = "";
	private String ver = "";
	private String delDt = "";
	private String delYn = "";
	private String pushUseYn = "";
	private String terModlInfo = "";
	private String empNo = "";
	private String regMember = "";
	private String title = "";
	private String description = "";
	private String openRange = "";
	private String sndTgtTyp = "";
	private String sndTgt= "";
	private String osTyp = "";
	private String pushTitl = "";
	private String pushCnts = "";
	private String sndSts = "";
	private String sndQty = "";
	private String regrId = "";
	private String pushSndYn = "";
	
	
	public String getSndTgt() {
		return sndTgt;
	}
	public void setSndTgt(String sndTgt) {
		this.sndTgt = sndTgt;
	}
	public String getOsTyp() {
		return osTyp;
	}
	public void setOsTyp(String osTyp) {
		this.osTyp = osTyp;
	}
	public String getPushTitl() {
		return pushTitl;
	}
	public void setPushTitl(String pushTitl) {
		this.pushTitl = pushTitl;
	}
	public String getPushCnts() {
		return pushCnts;
	}
	public void setPushCnts(String pushCnts) {
		this.pushCnts = pushCnts;
	}
	public String getSndSts() {
		return sndSts;
	}
	public void setSndSts(String sndSts) {
		this.sndSts = sndSts;
	}
	public String getSndQty() {
		return sndQty;
	}
	public void setSndQty(String sndQty) {
		this.sndQty = sndQty;
	}
	public String getRegrId() {
		return regrId;
	}
	public void setRegrId(String regrId) {
		this.regrId = regrId;
	}
	public String getPushSndYn() {
		return pushSndYn;
	}
	public void setPushSndYn(String pushSndYn) {
		this.pushSndYn = pushSndYn;
	}
	public String getSndTgtTyp() {
		return sndTgtTyp;
	}
	public void setSndTgtTyp(String sndTgtTyp) {
		this.sndTgtTyp = sndTgtTyp;
	}
	public String getOpenRange() {
		return openRange;
	}
	public void setOpenRange(String openRange) {
		this.openRange = openRange;
	}
	public String getRegMember() {
		return regMember;
	}
	public void setRegMember(String regMember) {
		this.regMember = regMember;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmpNo() {
		return empNo;
	}
	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}
	public String getFrstConnDt() {
		return frstConnDt;
	}
	public void setFrstConnDt(String frstConnDt) {
		this.frstConnDt = frstConnDt;
	}
	public String getDelDt() {
		return delDt;
	}
	public void setDelDt(String delDt) {
		this.delDt = delDt;
	}
	public String getDelYn() {
		return delYn;
	}
	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}
	public String getPushUseYn() {
		return pushUseYn;
	}
	public void setPushUseYn(String pushUseYn) {
		this.pushUseYn = pushUseYn;
	}
	public String getAppPushKey() {
		return appPushKey;
	}
	public void setAppPushKey(String appPushKey) {
		this.appPushKey = appPushKey;
	}
	public String getOsAffltn() {
		return osAffltn;
	}
	public void setOsAffltn(String osAffltn) {
		this.osAffltn = osAffltn;
	}
	public String getVer() {
		return ver;
	}
	public void setVer(String ver) {
		this.ver = ver;
	}
	public String getTerModlInfo() {
		return terModlInfo;
	}
	public void setTerModlInfo(String terModlInfo) {
		this.terModlInfo = terModlInfo;
	}


	
}
