package com.mns.sdoit.push.controller;



//import java.lang.ProcessBuilder.Redirect;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mns.sdoit.bbs.model.BbsModel;
import com.mns.sdoit.login.model.MemberModel;
import com.mns.sdoit.push.model.PushModel;
import com.mns.sdoit.push.service.PushService;

@Controller
public class PushController {
	
	private static final Logger logger = LoggerFactory.getLogger(PushController.class);
	
	@Autowired
	private PushService pushService;

	/**
	 * �뒪�궡 �샇異� �뫖�떆�궎 ���옣 而⑦듃濡ㅻ윭
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = {"/appkey"}, method ={RequestMethod.GET, RequestMethod.POST})
	public String saveAppkey(@RequestParam String appPushKey, @RequestParam String osAffltn, @RequestParam String ver, @RequestParam String terModlInfo, HttpServletRequest request ) throws Exception {
		logger.debug("## PushController - appkey");
		
		HttpSession session		= request.getSession();
		MemberModel memberInfo	= (MemberModel)session.getAttribute("member");
		String empNo 			= memberInfo.getEmpNo();
		PushModel pushModel = new PushModel();
		pushModel.setAppPushKey(appPushKey);
		pushModel.setOsAffltn(osAffltn);
		pushModel.setVer(ver);
		pushModel.setTerModlInfo(terModlInfo);
		pushModel.setEmpNo(empNo);
		
		try{
			pushService.insertAppInfo(pushModel);	
		}catch(Exception e){
			e.printStackTrace();
		}
		String returnURL = "/main";
		return "redirect:"+returnURL;
	}
	@RequestMapping(value = {"/pushAddGroupProcess"}, method ={RequestMethod.GET, RequestMethod.POST})
	public String pushAddGroupProcess(HttpServletRequest request) throws Exception {
		logger.debug("## PushController - pushAddGroupProcess");
		
		HttpSession session		= request.getSession();
		MemberModel memberInfo	= (MemberModel)session.getAttribute("member");
		String empNo 			= memberInfo.getEmpNo();
		String regMember = request.getParameter("regMember");
		String title = request.getParameter("title");
		String desc = request.getParameter("description");
		String openRange = request.getParameter("openRange");
		
		List<PushModel> list = null;
		openRange = "T01";
		//list = 	pushService.selectPushTgtList(openRange);
		logger.info(list.toString());
		PushModel pushModel = new PushModel();
		for(int i=0; i<list.size(); i++){
			pushModel.setSndTgtTyp("MEMBER");
			pushModel.setSndTgt(list.get(i).getAppPushKey());
			pushModel.setOsTyp("Android");
			pushModel.setPushTitl("ㅋㅋㅋ");
			pushModel.setPushCnts("ㅠㅠㅠ");
			pushModel.setSndSts("1");
			pushModel.setSndQty("1");
			pushModel.setRegrId("관리자");
			pushModel.setPushSndYn("Y");
			pushService.insertPushSndInfo(pushModel);
		}
		/*pushModel.setAppPushKey(appPushKey);
		pushModel.setOsAffltn(osAffltn);
		pushModel.setVer(ver);
		pushModel.setTerModlInfo(terModlInfo);
		pushModel.setEmpNo(empNo);*/

		String returnURL = "/main";
		return "redirect:"+returnURL;
	}
	
}
