package com.mns.sdoit.push.dao;

import java.util.HashMap;
import java.util.List;

import com.mns.sdoit.login.model.MemberModel;
import com.mns.sdoit.push.model.PushModel;





public interface PushDao  {

	void insertAppInfo(PushModel pushModel);

	List<PushModel> selectPushTgtList(HashMap<String, String> param);

	void insertPushSndInfo(PushModel pushModel);

}
