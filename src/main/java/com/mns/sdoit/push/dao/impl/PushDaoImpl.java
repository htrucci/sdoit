package com.mns.sdoit.push.dao.impl;


import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mns.sdoit.common.dao.BaseDao;
import com.mns.sdoit.push.dao.PushDao;
import com.mns.sdoit.push.model.PushModel;

@Repository
public class PushDaoImpl extends BaseDao implements PushDao {

	@Override
	public void insertAppInfo(PushModel pushModel) {
		getSqlMapClientTemplate().insert("PUSH.insertAppInfo", pushModel);
		
	}

	@Override
	public List<PushModel> selectPushTgtList(HashMap<String, String> param) {
		
		return getSqlMapClientTemplate().queryForList("PUSH.selectPushTgtList", param);
		
		
	}

	@Override
	public void insertPushSndInfo(PushModel pushModel) {
		getSqlMapClientTemplate().insert("PUSH.insertPushSndInfo", pushModel);
		
	}
}
