$(document).ready(function() {
	$(document).on("click", "[appCallbackType=pin]", function() {
		$(this).attr("active", "Y");
	});
});

function AppKeySave(appkey) {
	var appHref = "sdoit://appkeysave?callback=CallbackAppKeySave&key=" + appkey;
	location.href = appHref;
}

function CallbackDeviceinfo(appPushKey, osAffltn, ver, terModlInfo) {
	$("input[name=appPushKey]").val(appPushKey);
	$("input[name=osAffltn]").val(osAffltn);
	$("input[name=(ver]").val(ver);
	$("input[name=terModlInfo]").val(terModlInfo);
}