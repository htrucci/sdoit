/**
 * 세션정보 체크
 */
var invalidSessionValue = false;
function check_session(){
	var url = "/checkSession";
	$.ajax({
		type : "POST",
		url  : url,
		cashe : false,
		async : false,
		dataType : "json",
		success : function(data) {
			if(data.resultCode == '00'){
				invalidSessionValue = true;
			}
		},
		error : function(request,textStatus,errorThrown){
			alert("check_session()"+textStatus);
		}
	});
	return invalidSessionValue;
}

/**
 * 페이지 이동
 * @param returnURL
 */
function goPage(returnURL){
	location.href = "/goCertPage?returnURL="+returnURL;
}