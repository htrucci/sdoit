<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>  
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>S-DOIT</title>
	<meta name="viewport" content="width=device-width, initial-scale=0.7" />
	<!-- 부트스트랩 -->
	<script	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="/assets/js/headroom.min.js"></script>
	<script src="/assets/js/jQuery.headroom.min.js"></script>
	<script src="/assets/js/template.js"></script>
	<script src="/js/md5.js"></script>
	<script src="/js/bootstrap.js"></script>
	<script src="/js/respond.js"></script>
	<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="/js/jquery.ui.touch-punch.min.js"></script>
	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="/assets/css/bootstrap-theme.css"
	media="screen">
	<link rel="stylesheet" href="/assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="/assets/js/html5shiv.js"></script>
	<script src="/assets/js/respond.min.js"></script>
	<![endif]-->
	
	<script src="/js/dist/js/common/commUtil.js"></script>
</head>

<script type="text/javascript">
	$(document).ready(function(){

	});

	function writeBoard(){
		if( check_board() ){	
			var url 	= "/writeBoard";
			var param 	= $("form[name=boardFrm]").serialize();
			
			if(confirm("등록하시겠습니까?")){
				$.ajax({
					type : "POST",
					url  : url,
					cashe : false,
					data : param,
					async : false,
					dataType : "json",
					success : function(data) {
						if(data.resultCode == '00'){
							alert(data.resultMsg);
							location.href = "/main";
						}else{
							alert(data.resultMsg);
						}
					},
					error : function(request,textStatus,errorThrown){
						alert(textStatus);
					}
				});
			}
			
		}
	}
	
	function check_board(){
		
		if($("#title").val() == ""){
			alert("제목을 입력해주세요.");
			$("#title").focus();
			returnVal = false;
			return false;
		}
		
		if($("#description").val() == ""){
			alert("내용을 입력해주세요.");
			$("#description").focus();
			return false;
		}
		
		var startDt = $('#startDt').val();
		var endDt = $('#endDt').val();
		
		if(startDt=='' || endDt==''){
			alert('유효기간을 입력해주세요.');
			return false;
		}	
		
		var startDate = $('#startDt').val().split("-");
		var endDate  = $('#endDt').val().split("-");
		var sDate	   = new Date(startDate[0], startDate[1], startDate[2]).valueOf();
		var eDate	   = new Date(endDate[0], endDate[1], endDate[2]).valueOf();
				
		if( sDate > eDate )
		{
			alert('시작일 이후의 날짜를 선택해 주세요.');
			return false;
		}
		
		return true;
	}
</script>

<body>
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href="/main"><img src="/assets/images/logo.png" alt="Progressus HTML5 template"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li><a href="/main">메인</a></li>
					<li><a href="/getMyBbsList">내가 쓴 글</a></li>
					<li><a class="btn" href="/logout">Logout</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div> 
	<!-- /.navbar -->

	<header id="head" class="secondary"></header>

	<!-- container -->
	<div class="container">
	
		<div class="row">
			<!-- Article main content -->
			<article class="col-xs-12 maincontent">
				<br/><br/>
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3 class="thin text-center">이거 할래?</h3>
							<hr>
				
							<form id="boardFrm" name="boardFrm" method="post" class="form-horizontal">
								<div class="form-group">
									<label class="col-lg-3 control-label">제목</label>
									<div class="col-lg-5">
										<input type="text" class="form-control" name="title" id="title" maxlength="23"/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">내용</label>
									<div class="col-lg-5">
										<textarea class="form-control" rows="5" cols="50" name="description" id="description" maxlength="140"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">게시 범위</label>
									<div class="col-lg-5">
										<select name=openRange id="openRange" class="form-control">
											<option value="ALL">전체 공개</option>
											<option value="UNIT">유닛 공개</option>
											<option value="OFFICE">본부 공개</option>
											<option value="TEAM">팀 공개</option>
										</select>
									</div>
								</div>	
								<div class="form-group">
									<label class="col-lg-3 control-label">게시 기간</label>
									<div class="col-lg-5">
										<input type="date" name="startDt" id="startDt" step="1" />&nbsp;부터&nbsp;<input type="date" name="endDt" id="endDt" step="1" /> 까지<br/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-3 control-label">Push 전송 여부</label>
									<div class="col-lg-5">
										<input type="radio" name="pushYn" value="Y" />YES, I Do
										<br/><input type="radio" name="pushYn" value="N" checked="checked" />NO, I Don't
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-9 col-lg-offset-3">
										<button onclick="javascript:writeBoard(); return false;"><span class="glyphicon glyphicon-pencil">등록</span></button>
										<input type="reset" value="초기화" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</article>
			<!-- /Article -->
		</div>	<!-- /row -->
	</div>	<!-- /container -->
</body>
</html>