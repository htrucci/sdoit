<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%
String empNo = (String) request.getAttribute("empNo");


Cookie info = new Cookie("sdoit", empNo);    // 쿠키를 생성한다. 이름:testCookie, 값 : Hello Cookie

info.setMaxAge(365*24*60*60);                                 // 쿠키의 유효기간을 365일로 설정한다.
info.setPath("/");                                                    // 쿠키의 유효한 디렉토리를 "/" 로 설정한다.
response.addCookie(info);                                    // 클라이언트 응답에 쿠키를 추가한다.
 
%>

<!DOCTYPE html>  
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>S-DOIT</title>
	<meta name="viewport" content="width=device-width, initial-scale=0.7" />
	<!-- 부트스트랩 -->
	<script	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="/assets/js/headroom.min.js"></script>
	<script src="/assets/js/jQuery.headroom.min.js"></script>
	<script src="/assets/js/template.js"></script>
	<script src="/js/md5.js"></script>
	<script src="/js/bootstrap.js"></script>
	<script src="/js/respond.js"></script>
	<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="/js/jquery.ui.touch-punch.min.js"></script>
	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="/assets/css/bootstrap-theme.css"
	media="screen">
	<link rel="stylesheet" href="/assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="/assets/js/html5shiv.js"></script>
	<script src="/assets/js/respond.min.js"></script>
	<![endif]-->

	<script type="text/javascript">
	var selectedBbsIdx = 0;
	var bbsListSize = ${bbsListSize};
	
	  $(document).ready(function() {
		  	
		  if( $('#bbsListDragDiv').height() > $('#doItListDropDiv').height() ){
			  $('#doItListDropDiv').height($('#bbsListDragDiv').height());
		  }
		  
		  if( bbsListSize > 0 ){
		  		$( "#bbsListDragDiv > div" ).draggable({
			        connectToSortable: "#doitListUl",
			        revert: "invalid",
			        zIndex: 9999,
		        	drag: function() {
		        		selectedBbsIdx = $(this).find("input[name='idx']").val();
		            }
			      });
		  	}
		    $( "#doItListDropDiv" ).droppable({
		    	drop: function() {
		    		if(selectedBbsIdx > 0) {
		    			$.ajax({
		    				url : "/insertDo",
		    				type : "POST",
		    				data : "bbsIdx=" + selectedBbsIdx,
		    				dataType : "json",
		    				cache : false,
		    				async : false,
		    				success: function(data){
		    					console.log(data);
		    					if( data.RESULT == 'SUCCESS' ){
		    						location.reload();
		    					}else {
		    						alert(data.RESULT);
		    					}
		    				},
		    				error : function(xhr, status, error){
		    					alert(error);
		    				}
		    			});
		    		}
	            }
		    });
		    
		    $( "ul, li" ).disableSelection();
	  });
	  
	  function cancelDoit( idx ){
		  console.log('cancelDoit - '+idx);
		  $.ajax({
				url : "/cancelDo",
				type : "POST",
				data : "bbsIdx=" + idx,
				dataType : "json",
				cache : false,
				async : false,
				success: function(data){
					if( data.RESULT == 'SUCCESS' ){
						location.reload();
					}else {
						alert(data.RESULT);
					}
				},
				error : function(xhr, status, error){
					alert(error);
				}
			});
	  }
	  
	  function goMainView(){
		  var filterParam  = $('#filterParam option:selected').val();
		  location.href = '/main?filterParam='+filterParam;
	  }
	  
	</script>
	<style type="text/css">
	@media all and (max-width: 500px) {
		.row { 
			width:140%;
		}
	}
	</style>
</head>

<body>
<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom">
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/main"><img
					src="/assets/images/logo.png" ></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li><a href="/writeBoardForm">글 등록</a></li>
					<li><a href="/getMyBbsList">내가 쓴 글</a></li>
					<li><a class="btn" href="/logout">Logout</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
	<!-- /.navbar -->
	
	<header id="head" class="secondary"></header>
	
	
	<div class="container">
		<div class="row">
		    <div class="col-lg-6 col-md-6 col-xs-6">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h3 class="panel-title">할래?&nbsp;&nbsp;
		                	 <select name="filterParam" id="filterParam" onchange="javascript:goMainView();">
								<option value="ALL" ${(filterParam == 'ALL')? ' selected' : '' }>전체 공개</option>
								<option value="UNIT" ${(filterParam == 'UNIT')? ' selected' : '' }>유닛 공개</option>
								<option value="OFFICE" ${(filterParam == 'OFFICE')? ' selected' : '' }>본부 공개</option>
								<option value="TEAM" ${(filterParam == 'TEAM')? ' selected' : '' }>팀 공개</option>
							</select>
							
		                </h3>
		            </div>
		            
		            <div class="panel-body">
		            
		            	<!-- Drag 영역이 시작 -->
			            <div id="bbsListDragDiv">
			            	<c:if test="${bbsList == null || fn:length(bbsList) == 0}">
			            		<div class="panel panel-danger">
				            		<div class="panel-heading">
								        <h3 class="panel-title">오늘의 할일은 없네요..</h3>
								    </div>
								    <div class="panel-body">
								    	할일을 등록해 보세요.
								    </div>
							    </div>
			            	</c:if>
			            	
			            	<c:if test="${bbsList != null && fn:length(bbsList) > 0 }">
			            		 <c:forEach items="${bbsList}" var="bbs" varStatus="stat">
			            		 	<div class="panel panel-success">
									    <div class="panel-heading">
									        <strong><c:out value="${bbs.title}" /></strong>
									    </div>
									    <div class="panel-body">
								    		<c:out value="${bbs.description}" /><br/>
									    	
									    </div>
									    <div class="panel-footer">
									    	<span><c:out value="${bbs.endDt}" /> 까지.</span>
									    	<span style="float:right;">[<c:out value="${bbs.regMamberName}" />]</span>
									    </div>
										<input type="hidden" name="idx" value="${bbs.idx}" />
									</div>
								</c:forEach>
			            	</c:if>
				        </div>
				        
			        </div>
		        </div>
		    </div>
		    
		    <div class="col-lg-6 col-md-6 col-xs-6">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h3 class="panel-title">할래!</h3>
		            </div>
		            
		            <div class="panel-body">
	
		            	<!-- Drop 영역이 시작 -->
		            	<div id="doItListDropDiv">
		            	
			            	<c:if test="${doItList == null || fn:length(doItList) == 0}">
			            		<div class="panel panel-danger">
									<div class="panel-heading">
								        <h3 class="panel-title">오늘의 할일은 없네요..</h3>
								    </div>
								    <div class="panel-body">
								    	할일을 등록해 보세요.
								    </div>
							    </div>
			            	</c:if>
						
							<c:if test="${doItList != null && fn:length(doItList) > 0 }">
								<c:forEach items="${doItList}" var="doIt" varStatus="stat">
									<div class="panel panel-info">
									    <div class="panel-heading">
									        <span><strong><c:out value="${doIt.title}" /></strong></span>
									        <span style="float:right;"><c:out value="${doIt.regMamberName}" /></span>
									    </div>
									    <div class="panel-body">
									    	<c:out value="${doIt.description}" /><br/>
									    </div>
									    <div class="panel-footer">
									    	<span><c:out value="${doIt.endDt}" /> 까지.&nbsp; 
									    	<button type="button" onclick="javascript:cancelDoit('${doIt.idx}');">안할래</button></span>
									    </div>
									    <input type="hidden" name="idx" value="${doIt.idx}" />
								    </div>
								</c:forEach>
							</c:if>
							
		            	</div>
		            	
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</body>
</html>