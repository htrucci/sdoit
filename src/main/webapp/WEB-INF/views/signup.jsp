<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<html>
<head>
<meta charset="UTF-8">
<title>S-DOIT</title>
<meta name="viewport" content="width=device-width, initial-scale=0.8" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/assets/js/headroom.min.js"></script>
<script src="/assets/js/jQuery.headroom.min.js"></script>
<script src="/assets/js/template.js"></script>
<script src="/js/md5.js"></script>
<script src="/js/respond.js"></script>
<script type="text/javascript" src="/js/dist/js/formValidation.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/dist/js/formValidation.js"></script>
<script type="text/javascript" src="/js/dist/js/framework/bootstrap.js"></script>

<link rel="shortcut icon" href="/assets/images/gt_favicon.png">
<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/font-awesome.min.css">

<!-- Custom styles for our template -->
<link rel="stylesheet" href="/assets/css/bootstrap-theme.css" media="screen">
<link rel="stylesheet" href="/assets/css/main.css">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="/assets/js/html5shiv.js"></script>
	<script src="/assets/js/respond.min.js"></script>
	<![endif]-->
<script type="text/javascript">
function join(){
	if(!chkData()){
		return;
	}
	var thisForm = document.getElementById("joinForm");
	var empNo = jQuery.trim(thisForm.empNo.value);
	var password = jQuery.trim(MD5(thisForm.password.value));
	var name = jQuery.trim(thisForm.name.value);
	var unit = $("#unit option:selected").val();
	var office = $("#office option:selected").val();
	var team = $("#team option:selected").val();
	var param = 'empNo='+empNo+'&pw='+password+'&name='+name+'&unit='+unit+'&office='+office+'&team='+team;
	
	$.ajax({
		url : "/login/joinMember",
		type : "POST",
		data : param,
		dataType : "json",
		cache : false,
		async : false,
		success: function(data){
			if( data.RESULT == 'SUCCESS' ){
				alert("가입이 완료되었습니다! 다시 로그인 해 주세요.");
				location.href="/login/loginView";
			}
		},
		error : function(xhr, status, error){
			alert(error);
		}
	});
}

	$(document)
			.ready(
					function() {
						getCodeList("unit", "ROOT");
						$("#unit, #office").on("change", getCodeList);
						// Generate a simple captcha
						function randomNumber(min, max) {
							return Math.floor(Math.random() * (max - min + 1)
									+ min);
						}
						;
						$('#captchaOperation').html(
								[ randomNumber(1, 100), '+',
										randomNumber(1, 10), '=' ].join(' '));

						$('#joinForm')
								.formValidation(
										{
											message : '올바르지 않은 값입니다.',
											fields : {
												empNo : {
													message : '필수로 입력하세요.',
													validators : {
														notEmpty : {
															message : '필수로 입력하세요.'
														},
														different : {
															field : 'password',
															message : '패스워드와 같을 수 없습니다.'
														}
													}
												},
												password : {
													validators : {
														notEmpty : {
															message : '필수로 입력하세요.'
														},
														identical : {
															field : 'confirmPassword',
															message : '패스워드가 같지않습니다.'
														},
														different : {
															field : 'empNo',
															message : '패스워드는 이름과 같을 수 없습니다.'
														}
													}
												},
												confirmPassword : {
													validators : {
														notEmpty : {
															message : '필수로 입력하세요.'
														},
														identical : {
															field : 'password',
															message : '패스워드가 같지않습니다.'
														},
														different : {
															field : 'empNo',
															message : '패스워드는 이름과 같을 수 없습니다.'
														}
													}
												},
												captcha : {
													validators : {
														callback : {
															message : 'Wrong answer',
															callback : function(
																	value,
																	validator) {
																var items = $(
																		'#captchaOperation')
																		.html()
																		.split(
																				' '), sum = parseInt(items[0])
																		+ parseInt(items[2]);
																return value == sum;
															}
														}
													}
												}
											}
										});
					});
	
function getCodeList(targetArea, parentCode) {
	var $area = (typeof targetArea != "string") ? $(this).closest(".form-group").next().find("select") : $("#" + targetArea);
	var $value = (typeof parentCode != "string") ? $(this).find("option:selected").val() : parentCode;

	$.ajax({
		url: "/getCodeList",
		type: "GET",
		data: "parentCode=" + $value,
		dataType: "json",
		cache: false,
		async: false,
		success: function(data) {
			for(var i = 0 ; i < data.length ; i++) {
				if(i == 0) {
					$area.html("");	
				}
				$area.append("<option value=" + data[i].code + ">" + data[i].name + "</option>");
			}
			
			if($area.attr("id") == "unit") {
				getCodeList("office", data[0].code);
			}else if($area.attr("id") == "office"){
				getCodeList("team", data[0].code);
			}
		},
		error: function(xhr, status, error) {
			alert(error);
		}
	});
}

function chkData(){
	if($("#empNo").val() == ""){
		alert("사번을 입력하세요.");
		$("#empNo").focus();
		return false;
	}
	if(!duplicateIdCheckFlag){
		alert("ID Check를 진행하세요.");
		$("#empNo").focus();
		return false;
	}
	
	if($("#name").val() == ""){
		alert("이름을 입력하세요.");
		$("#name").focus();
		return false;
	}
	if($("#password").val() == ""){
		alert("비밀번호를 입력하세요.");
		$("#password").focus();
		return false;
	}
	if(!post_check()){
		$("#password").focus();
		return false;
	}
	
	if($("#captcha").val() == ""){
		alert("덧셈 테스트를 진행해주세요.");
		$("#captcha").focus();
		return false;
	}
	return true;
}

var duplicateIdCheckFlag = false;
function duplicateIdCheck(){
	if($("#empNo").val() == ""){
		alert("사번을 입력하세요.");
		$("#empNo").focus();
		duplicateIdCheckFlag = false;
		return;
	}
	
	if($("#empNo").val().length < 6 || $("#empNo").val().length > 16){
		alert("사번은 6자리 이상 12자리 이하입니다.");
		$("#empNo").focus();
		duplicateIdCheckFlag = false;
		return;
	}
	
	var chktext = /[`~!@#$%^&*|\\\'\";:\/?]/gi;
	 
	if (chktext.test($("#empNo").val())) {
     	alert("특수문자는 입력하실 수 없습니다.?");
     	//$("#empNo").val("");
     	$("#empNo").focus();
     	duplicateIdCheckFlag = false;
     	return;
	}
	
	$.ajax({
		url: "/duplicateIdCheck",
		type: "GET",
		data: "empNo=" + $("#empNo").val(),
		dataType: "json",
		cache: false,
		async: false,
		success: function(data) {
			if( data.RESULT == 'SUCCESS' ){
				alert("사용할 수 있는 사번입니다.");
				duplicateIdCheckFlag = true;
			}else{
				alert("이미 사용중이거나, 사용할 수 없는 사번입니다.");
				duplicateIdCheckFlag = false;
			}
		},
		error: function(xhr, status, error) {
			alert(error);
			duplicateIdCheckFlag = false;
		}
	});
}

function post_check()
{
   // 비밀번호(패스워드) 유효성 체크 (문자, 숫자, 특수문자의 조합으로 6~16자리)
  var ObjUserPassword = $("#password").val();

  if(ObjUserPassword.length<6 || ObjUserPassword.length>16) {
    alert("비밀번호는 6~16자까지 입력해 주세요.");
    return false;
  }
 
/*  if(!ObjUserPassword.match(/([a-zA-Z0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z0-9])/)) {
    alert("비밀번호는 영문,숫자,특수문자(!@$%^&* 만 허용)를 사용하여 6~16자까지, 영문은 대소문자를 구분합니다.");
    return false;
  }
 
  var SamePass_0 = 0; //동일문자 카운트
  var SamePass_1 = 0; //연속성(+) 카운드
  var SamePass_2 = 0; //연속성(-) 카운드
 
  for(var i=0; i < ObjUserPassword.length; i++) {
    var chr_pass_0 = ObjUserPassword.charAt(i);
    var chr_pass_1 = ObjUserPassword.charAt(i+1);
    
    //동일문자 카운트
    if(chr_pass_0 == chr_pass_1) {
      SamePass_0 = SamePass_0 + 1
    }
    
    var chr_pass_2 = ObjUserPassword.charAt(i+2);

    //연속성(+) 카운드
    if(chr_pass_0.charCodeAt(0) - chr_pass_1.charCodeAt(0) == 1 && chr_pass_1.charCodeAt(0) - chr_pass_2.charCodeAt(0) == 1) {
      SamePass_1 = SamePass_1 + 1
    }
    
    //연속성(-) 카운드
    if(chr_pass_0.charCodeAt(0) - chr_pass_1.charCodeAt(0) == -1 && chr_pass_1.charCodeAt(0) - chr_pass_2.charCodeAt(0) == -1) {
      SamePass_2 = SamePass_2 + 1
    }
  }
  if(SamePass_0 > 1) {
    alert("동일문자를 3번 이상 사용할 수 없습니다.");
    return false;
  }
 
  if(SamePass_1 > 1 || SamePass_2 > 1 ) {
    alert("연속된 문자열(123 또는 321, abc, cba 등)을\n 3자 이상 사용 할 수 없습니다.");
    return false;
  } */
  
  return true;
}
</script>	
</head>

<body>
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href="/"><img src="/assets/images/logo.png" alt="Progressus HTML5 template"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li><a href="/">Home</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div> 
	<!-- /.navbar -->

	<header id="head" class="secondary"></header>

	<!-- container -->
	<div class="container">


		<div class="row">
			
			<!-- Article main content -->
			<article class="col-xs-12 maincontent">
				<br/><br/>
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3 class="thin text-center">회원 정보를 입력해 주세요</h3>
							<hr>
							
		<form id="joinForm" method="post" class="form-horizontal">
			<div class="form-group">
				<label class="col-lg-3 control-label">사번</label>
				<div class="col-lg-5">
					<input type="text" class="form-control" name="empNo" id="empNo" maxlength="12"/>
					<a href="#" onclick="javascript:duplicateIdCheck();"><span class="glyphicon glyphicon-pencil">ID Check</span></a>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-lg-3 control-label">이름</label>
				<div class="col-lg-5">
					<input type="text" class="form-control" name="name" id="name" maxlength="10"/>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-lg-3 control-label">비밀번호</label>
				<div class="col-lg-5">
					<input type="password" class="form-control" name="password" id="password" maxlength="16"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-3 control-label">비밀번호 확인</label>
				<div class="col-lg-5">
					<input type="password" class="form-control" name="confirmPassword" maxlength="16"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-3 control-label">유닛</label>
				<div class="col-lg-5">
					<select name="unitCode" id="unit" class="form-control">
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-3 control-label">본부</label>
				<div class="col-lg-5">
					<select name="officeCode" id="office" class="form-control">
					</select>
				  	<c:remove var="officeCount"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-3 control-label">팀</label>
				<div class="col-lg-5">
					<c:set var="teamCount" value="0" />
					<select name="teamCode" id="team" class="form-control">
					</select>
				</div>
			</div>						
			<div class="form-group">
				<label class="col-lg-3 control-label" id="captchaOperation"></label>
				<div class="col-lg-2">
					<input type="text" class="form-control" name="captcha" id="captcha"/>
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-9 col-lg-offset-3" style="text-align: center;">
					<a href="javascript:join();"  class="btn btn-primary" type="submit" role="button">회원가입</a>
				</div>
			</div>
		</form>
						</div>
					</div>

				</div>
				
			</article>
			<!-- /Article -->

		</div>
	</div>	<!-- /container -->
	

	<footer id="footer" >
		<div class="footer1">
			<div class="container">
				<div class="row">
					<br/><br/>
					<div class="col-md-3 widget">
						<h3 class="widget-title">Contact</h3>
						<div class="widget-body">
							<p>(주) 엠앤서비스 시스템개발3팀<br/>
							서울특별시 중구 흥인동 132번지 준타워빌딩 8층
							</p>	
						</div>
					</div>
				</div> <!-- /row of widgets -->
			</div>
		</div>
	</footer>		
		
</body>
</html>