<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<html>
<head>
<meta charset="UTF-8">
<title>S-DOIT</title>
<meta name="viewport" content="width=device-width, initial-scale=0.8" />

<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/assets/js/headroom.min.js"></script>
<script src="/assets/js/jQuery.headroom.min.js"></script>
<script src="/assets/js/template.js"></script>
<script src="/js/md5.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/respond.js"></script>

<link rel="shortcut icon" href="/assets/images/gt_favicon.png">

<link rel="stylesheet" media="screen"
	href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/font-awesome.min.css">

<!-- Custom styles for our template -->
<link rel="stylesheet" href="/assets/css/bootstrap-theme.css"
	media="screen">
<link rel="stylesheet" href="/assets/css/main.css">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="/assets/js/html5shiv.js"></script>
	<script src="/assets/js/respond.min.js"></script>
	<![endif]-->
<script type="text/javascript">
	var currentOS;
	var mobile = (/iphone|ipad|ipod|android/i.test(navigator.userAgent.toLowerCase()));
	 
	if (mobile) {
		// 유저에이전트를 불러와서 OS를 구분합니다.
		var userAgent = navigator.userAgent.toLowerCase();
		if (userAgent.search("android") > -1)
			currentOS = "android";
		else if ((userAgent.search("iphone") > -1) || (userAgent.search("ipod") > -1)
					|| (userAgent.search("ipad") > -1))
			currentOS = "ios";
		else
			currentOS = "else";
	} else {
		// 모바일이 아닐 때
		currentOS = "nomobile";
	}
	function checkAppInstall() {
		// 앱에 설정해놓은 커스텀 스킴. 여기선 "customScheme"
		var url = "sdoit://deviceinfo?callback=CallbackDeviceinfo";
		if(currentOS == "android") {
			// 안드로이드는 미리 만들어둔 iframe에 
			setTimeout( function() {
				goMarket();	
				return false;
			}, 2000);
			var invisible_div = document.getElementById("invisible_div");
			invisible_div.innerHTML = "<iframe src=" + url + " onload=goMarket()></iframe>";
			/* location.href = url; */
			
		} else if(currentOS == "ios") {
			setTimeout( function() {
				goMarket();
			}, 1000);
			location.href = url;
		} else {
			alert("안드로이드와 아이폰에서만 사용 가능");
		}
		return false;
	}
	 
	// 마켓 이동
	function goMarket() {
		if(currentOS == "android") {
			location.href="/main";
		} else if(currentOS == "ios") {
			location.href="/main";
		} else {
			/* 기타 OS일 때 */
		}
	}

	$(document).ready(function() {

		$('.loginInput').keypress(function(e) {
			if (e.keyCode == 13) {
				login();
			}
		});

		$('#loginBtn').click(function() {
			login();
		});

	});
	function CallbackDeviceinfo(appPushKey, osAffltn, ver, terModlInfo){
		$("input[name=appPushKey]").val(appPushKey);
		$("input[name=osAffltn]").val(osAffltn);
		$("input[name=ver]").val(ver);
		$("input[name=terModlInfo]").val(terModlInfo);
		document.frmUpdate.submit();
	}
	function confirms() {
		var thisForm = document.getElementById("loginFrm");
		var empNo = jQuery.trim(thisForm.empNo.value);
		var password = thisForm.pw.value;
		thisForm.empNo.value = empNo;
		if (empNo == "" || empNo == $("input[name='empNo']").attr("placeholder")) {
			alert("사번을 입력하세요.");
			thisForm.empNo.focus();
			return false;
		}
		if (password == "" || password == $("input[name='pw']").attr("placeholder")) {
			alert("비밀번호를 입력해 주세요.");
			thisForm.pw.focus();
			return false;
		}
	}

	function login() {
		if (confirms() != false) {

			var thisForm = document.getElementById("loginFrm");
			var empNo = jQuery.trim(thisForm.empNo.value);
			var password = jQuery.trim(MD5(thisForm.pw.value));
			var param = 'empNo=' + empNo + '&pw=' + password;
			$.ajax({ url : "/login/login", type : "POST", data : param, dataType : "json", cache : false, async : false, success : function(data) {
					if (data.RESULT == 'SUCCESS') {
						checkAppInstall(); // 앱키 저장 스킴 호출	
						//location.href = "/main";
					} else if (data.RESULT == 'INVALID_PASSWORD') {
						alert('패스워드를 확인해 주세요');
					} else if (data.RESULT == 'ID_NOT_EXIST') {
						alert('회원이 아닙니다.');
					} else {
						alert('로그인에 실패했습니다.\n관리자에게 문의해 주새요.');
					}

				}, error : function(xhr, status, error) {
					alert(error);
				} });
		}
	}
</script>

<style type="text/css">
	@import url(http://fonts.googleapis.com/earlyaccess/nanumpenscript.css);
	.nps {font-family: 'Nanum Pen Script';}
</style>

</head>

<body>
	<form name="frmUpdate" id="frmUpdate" action="/appkey.do">
		<input type="hidden" name="appPushKey" value=""/>
		<input type="hidden" name="osAffltn" value=""/>
		<input type="hidden" name="ver" value=""/>
		<input type="hidden" name="terModlInfo" value=""/>
	</form>
	<!-- Fixed navbar -->
	<div id="invisible_div" style="display:none;"></div>
	<div class="navbar navbar-inverse navbar-fixed-top headroom">
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/"><img
					src="/assets/images/logo.png" alt="Progressus HTML5 template"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li><a href="/">Home</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
	<!-- /.navbar -->
	<!-- display 속성을 두어 숨어있는 div를 미리 만들어 놓습니다. 이 div에 iframe이 생성됩니다. -->
	<header id="head" class="secondary"></header>

	<!-- container -->
	<div class="container">

		<div class="row">
			<br/><br/>
			<!-- Article main content -->
			<article class="col-xs-12 maincontent">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3 class="thin text-center">계정 정보를 입력해 주세요</h3>
							<hr>
							<form class="form-signin" id="loginFrm">

								<h2 class="nps">할래? 할래!</h2>
								<div class="top-margin">
									<input type="text" id="empNo" name="empNo" class="form-control"	placeholder="사번" autofocus>
								</div>
								<br/> <input type="password" id="pw" name="pw" class="form-control"	placeholder="PW">
								<hr>
								<a href="javascript:login();" class="btn btn-lg btn-primary btn-block" type="submit"	role="button">로그인</a> 
								<a href="/login/joinView" class="btn btn-lg btn-primary btn-block" role="button">회원가입</a>
							</form>

						</div>
					</div>

				</div>

			</article>
			<!-- /Article -->

		</div>
	</div>
	<!-- /container -->
<br/><br/>
	<!-- /Header -->
	<footer id="footer" >
		<div class="footer1">
			<div class="container">
				<div class="row">
					<br/><br/>
					<div class="col-md-3 widget">
						<h3 class="widget-title">Contact</h3>
						<div class="widget-body">
							<p>(주) 엠앤서비스 시스템개발3팀<br/>
							서울특별시 중구 흥인동 132번지 준타워빌딩 8층
							</p>	
						</div>
					</div>
				</div> <!-- /row of widgets -->
			</div>
		</div>
	</footer>	

</body>
</html>