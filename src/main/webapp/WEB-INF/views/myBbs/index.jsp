<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>  
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<title>S-DOIT</title>
	<meta name="viewport" content="width=device-width, initial-scale=0.7" />
	<!-- 부트스트랩 -->
	<script	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="/assets/js/headroom.min.js"></script>
	<script src="/assets/js/jQuery.headroom.min.js"></script>
	<script src="/assets/js/template.js"></script>
	<script src="/js/md5.js"></script>
	<script src="/js/bootstrap.js"></script>
	<script src="/js/respond.js"></script>
	<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script src="/js/jquery.ui.touch-punch.min.js"></script>
	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="/assets/css/bootstrap-theme.css"
	media="screen">
	<link rel="stylesheet" href="/assets/css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="/assets/js/html5shiv.js"></script>
	<script src="/assets/js/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript">
	function getApplicantList(bbsIdx) {
		$.ajax({
			url : "/getApplicantList",
			type : "GET",
			data : "bbsIdx=" + bbsIdx,
			dataType : "html",
			cache : false,
			async : true,
			success: function(data){
				$("#applicantArea").html("").html(data);
				//$("#applicantArea").show("fast");
			},
			error : function(xhr, status, error){
				alert(error);
			}
		});
	}
	
	function deleteBbs( idx ){
		
		$.ajax({
			url : "/deleteBoard",
			type : "POST",
			data : "idx=" + idx,
			dataType : "json",
			cache : false,
			async : false,
			success: function(data){
				if(data.resultCode == '00'){
					alert(data.resultMsg);
					location.href = "/getMyBbsList";
				}else{
					alert(data.resultMsg);
				}
			},
			error : function(xhr, status, error){
				alert(error);
			}
		});
		
	}
	
	function closeListDiv(){
		//$("#applicantArea").hide("fast", function(){ $("#applicantArea").html("");} );
		$("#applicantArea").html("");
	}
	
	</script>
	<style type="text/css">
	@media all and (max-width: 500px) {
		.row { 
			width:140%;
		}
	}
	</style>
</head>

<body>
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom">
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span><span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/getMyBbsList"><img
					src="/assets/images/logo.png" alt="Progressus HTML5 template"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li><a href="/main">메인</a></li>
					<li><a href="/writeBoardForm">글 등록</a></li>
					<li><a class="btn" href="/logout">Logout</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
	<!-- /.navbar -->

	<header id="head" class="secondary"></header>
	
	<div class="container">
		<div class="row">
		
		    <div class="col-lg-6 col-md-6 col-xs-6">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                <h3 class="panel-title">내가 쓴 글</h3>
		            </div>
		            
		            <div class="panel-body">
		            	
		            	<c:if test="${myBbsList == null || fn:length(myBbsList) == 0}">
		            		<div class="panel panel-danger">
			            		<div class="panel-heading">
							        <h3 class="panel-title">내가 쓴 글은 없네요..</h3>
							    </div>
							    <div class="panel-body">
							    	할일을 등록해 보세요.
							    </div>
						    </div>
		            	</c:if>
		            	
		            	<c:if test="${myBbsList != null || fn:length(myBbsList) > 0 }">
		            		 <c:forEach items="${myBbsList}" var="myBbs" varStatus="stat">
		            		 	<div class="panel panel-success">
								    <div class="panel-heading">
								        <h3 class="panel-title">${myBbs.title}&nbsp;&nbsp;<span class="badge">${myBbs.doCnt}</span></h3>
								    </div>
								    <div class="panel-body">
								    	<c:out value="${myBbs.description}" /><br/>
								    </div>
								    <div class="panel-footer">
								    	<c:out value="${myBbs.endDt}" /> 까지.
								    </div>
								    <div class="panel-footer">
								    	<!-- // 15-04-30 수정 -->
										<button type="button" onclick="javascript:deleteBbs(${myBbs.idx});">글삭제</button> 
										<button type="button" onclick="javascript:getApplicantList(${myBbs.idx});">신청자 보기</button>
								    	<!-- 15-04-30 수정 :end // -->
								    </div>
								</div>
							</c:forEach>
		            	</c:if>
				        
			        </div>
		        </div>
		    </div>
		    
		    <div id="applicantArea"></div>
		</div>
	</div>
</body>
</html>