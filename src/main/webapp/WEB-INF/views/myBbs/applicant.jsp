<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="col-lg-6 col-md-6 col-xs-6">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">신청자 목록</h3>
        </div>
        
        <div class="panel-body">
        
        	<div class="panel panel-success">
	        	<c:if test="${applicantList == null || fn:length(applicantList) == 0}">
					    <ul>
					    	<li>신청자가 없습니다</li>
					    </ul>
	        	</c:if>
	        	
	        	<c:if test="${applicantList != null && fn:length(applicantList) > 0}">
					<ol>
				    	<c:forEach items="${applicantList}" var="applicant" varStatus="var">
				    		<li>[${applicant.teamName}]&nbsp;${applicant.memberName} </li>
				    	</c:forEach>
				    </ol>
	        	</c:if>
	        	
	        	<div class="panel-footer">
	        		<button type="button" onclick="javascript:closeListDiv();">목록 닫기</button> 
			    </div>
			</div>
      
     </div>
    </div>
</div>
